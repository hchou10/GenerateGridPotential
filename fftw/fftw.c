#include "fftw.h"
fftw_plan fftwr2c_on_grid(fftw_complex *out, size_t nx, size_t ny, size_t nz, double* data)
{
     return fftw_plan_dft_r2c_3d(nx, ny, nz, data, out, FFTW_ESTIMATE);
}

fftw_plan fftwc2r_on_grid(double* data, size_t nx, size_t ny, size_t nz, fftw_complex *out)
{
     return fftw_plan_dft_c2r_3d(nx, ny, nz, out, data, FFTW_ESTIMATE);
}

