#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "grid_reader.h"

int main(int argc, char** argv)
{
    double a = (double)atof(argv[3]);
    void* grid;
    grid = gridformat_reader(argv[1]);
    multiply_constant(grid, a);

    gridformat_writer(argv[2], grid); 
    grid_destroy(grid);
    return 0;
}
