#include <stdlib.h>
#include <stdio.h>
#include "grid_reader.h"
double sum(void* src)
{
    double* data = get_grid_data(src);
    size_t size = get_size(src);
    double** delta = get_grid_basis(src);
    double dv = delta[0][0]*delta[1][1]*delta[2][2];
    double f = 0.;
    for(size_t i = 0; i < size; ++i)
    {
        f += data[i]*dv;
    } 
    return f;
}

int main(int argc, const char** argv)
{
    char* input_file_name=argv[1];
    void* grid;
    grid = gridformat_reader(input_file_name);
    printf("%lf\n", sum(grid));
    grid_destroy(grid);
    return 0;
}
