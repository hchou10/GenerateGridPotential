#include <stdlib.h>
#include "grid_reader.h"
int main(int argc, char** argv)
{
    char* input = argv[1];
    char* output = argv[2];
    int nx = atoi(argv[3]);
    int ny = atoi(argv[4]);
    int nz = atoi(argv[5]);
    void* grid = gridformat_reader(input);
    slice_grid(&grid, nx,ny,nz);
    gridformat_writer(output, grid);
    grid_destroy(grid);
    return 0;
}
