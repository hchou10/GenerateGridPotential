#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "grid_reader.h"
#include <stdio.h>
static void parse_input(char*** input, int* num, char** args)
{
    size_t size = 4;
    *input = (char**)malloc(size*sizeof(char*));
    char** tmp = args; 
    size_t count = 0;
    while(*tmp != NULL)
    {
        (*input)[count++] = strdup(*tmp);
        if(count==size)
        {
            size <<= 1;
            *input = (char**)realloc(*input, size*sizeof(char*));
        }
        tmp++;
        if(strcmp(*tmp, "-o") == 0)
            break;
    }
    (*input)[count] = NULL;
    *num = count;
}

static void parse_string(char** argv, char*** input, 
int* num, char** output)
{
    char** ptr = argv;
    while(*ptr != NULL)
    {
        if(strcmp(*ptr, "-i") == 0)
        {
            parse_input(input, num, ++ptr);
        }
        else if(strcmp(*ptr, "-o") == 0)
        {
            *output = strdup(*(++ptr));
        }
        else
            ++ptr;
    }
}

int main(int argc, char** argv)
{
    char** input_file_name=NULL;
    char* output_file_name=NULL;
    int num = 0;
    parse_string(argv, &input_file_name, &num, &output_file_name);
    if(num != 2)
    {
        fprintf(stderr, "input error\n");
        exit(-1);
    }
    void** grid = malloc(sizeof(void*)*num);

    for(size_t i = 0; i < (size_t)num; ++i)
        grid[i] = gridformat_reader(input_file_name[i]);
    printf("%d\n", num);
    void* out_grid = copy_grid_format(grid[0]);
    sub_grids(out_grid, grid[0], grid[1]);

    gridformat_writer(output_file_name, out_grid); 
    for(size_t i = 0; i < (size_t)num; ++i)
    {
        grid_destroy(grid[i]);
        free(input_file_name[i]);
    }
    free(input_file_name);
    free(grid);
    grid_destroy(out_grid);
    free(output_file_name);
   
    return 0;
}
