#include "lenard_jones_kernel.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
struct lenard_jones_kernel
{
    double c6; 
    double c12;
};

void* lenard_jones_create(char* name)
{
    void* src = malloc(sizeof(lenard_jones_kernel));
    FILE* infile = fopen(name, "r");
    if(infile == NULL)
    {
        perror("Error in opening file:");
        free(src);
        return NULL;
    }
    char buffer[512];
    while(fgets(buffer, 512, infile) != NULL)
    {
         char* tmp = strtok(buffer, " ");
         if(strcmp(tmp, "C6") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((lenard_jones_kernel*)src)->c6 = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }
         else if(strcmp(tmp, "C12") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((lenard_jones_kernel*)src)->c12 = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }
         else
         {
             printf("Unrecognized pattern\n");
             free(src);
             return NULL;
         }
    }
    fclose(infile);
    return src;
}

double lenard_jones_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src)
{
    lenard_jones_kernel* kernel = (lenard_jones_kernel*)src;
    double pos[3];
    for(size_t dim = 0; dim < 3; ++dim)
        pos[dim] = basis[dim][0]*i + basis[dim][1]*j + basis[dim][2]*k + origin[dim];
    double d = pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2];
    d = d*d*d;
    if(d > 0.)
    {
        double e = -(kernel->c6)/d+(kernel->c12)/(d*d);
        return (e>100) ? 100 : e;
    }
    else
    {
        return 100;
    }
}

void lenard_jones_destroy(void* src)
{
    free(src);
}
