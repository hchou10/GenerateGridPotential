#include "debye_huckel_kernel.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
struct debye_huckel_kernel
{
    double screen_length; 
    double epsilon;
};

void* debye_huckel_create(char* name)
{
    void* src = malloc(sizeof(debye_huckel_kernel));
    FILE* infile = fopen(name, "r");
    if(infile == NULL)
    {
        perror("Error in opening file:");
        free(src);
        return NULL;
    }
    char buffer[512];
    while(fgets(buffer, 512, infile) != NULL)
    {
         char* tmp = strtok(buffer, " ");
         if(strcmp(tmp, "ScreenLength") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((debye_huckel_kernel*)src)->screen_length = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }
         else if(strcmp(tmp, "Dielectric") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((debye_huckel_kernel*)src)->epsilon = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }
         else
         {
             printf("Unrecognized pattern\n");
             free(src);
             return NULL;
         }
    }
    fclose(infile);
    return src;
}

double debye_huckel_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src)
{
    debye_huckel_kernel* kernel = (debye_huckel_kernel*)src;
    double pos[3];
    for(size_t dim = 0; dim < 3; ++dim)
        pos[dim] = basis[dim][0]*i + basis[dim][1]*j + basis[dim][2]*k + origin[dim];
    double d = sqrt(pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2]);
    if(d > 0.)
    {
        double e = exp(-d/kernel->screen_length)/d/kernel->epsilon;  
        return (e > 20.) ? 20.: e;
    }
    else
        return 20.;
}

void debye_huckel_destroy(void* src)
{
    free(src);
}
