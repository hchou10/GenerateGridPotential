#include "tabulated_pot_kernel.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
struct tabulated_pot_kernel
{
    int num;
    double* dist;
    double* pot;
};

void* tabulated_pot_create(char* name)
{
    void* src = malloc(sizeof(tabulated_pot_kernel));
    memset(src, 0, sizeof(tabulated_pot_kernel));
    int capacity = 128;

    ((tabulated_pot_kernel*)src)->dist = malloc(sizeof(double)*capacity); 
    ((tabulated_pot_kernel*)src)->pot  = malloc(sizeof(double)*capacity);
    FILE* infile = fopen(name, "r");
    if(infile == NULL)
    {
        perror("Error in opening file:");
        free(src);
        return NULL;
    }
    char buffer[512];
    while(fgets(buffer, 512, infile) != NULL)
    {
         char* tmp = strtok(buffer, " ");
         if(tmp == NULL)
             return NULL;
         ((tabulated_pot_kernel*)src)->dist[((tabulated_pot_kernel*)src)->num] = atof(tmp);
         tmp = strtok(NULL, " ");
         if(tmp == NULL)
             return NULL;
         ((tabulated_pot_kernel*)src)->pot[((tabulated_pot_kernel*)src)->num]  = atof(tmp);
         ((tabulated_pot_kernel*)src)->num++;
         if(((tabulated_pot_kernel*)src)->num == capacity)
         {
             capacity *= 2;
             ((tabulated_pot_kernel*)src)->dist = realloc(((tabulated_pot_kernel*)src)->dist, sizeof(double)*capacity);
             ((tabulated_pot_kernel*)src)->pot  = realloc(((tabulated_pot_kernel*)src)->pot,  sizeof(double)*capacity);

         }
    }
    ((tabulated_pot_kernel*)src)->dist = realloc(((tabulated_pot_kernel*)src)->dist, sizeof(double)*(((tabulated_pot_kernel*)src)->num));
    ((tabulated_pot_kernel*)src)->pot  = realloc(((tabulated_pot_kernel*)src)->pot,  sizeof(double)*(((tabulated_pot_kernel*)src)->num));
    fclose(infile);
    return src;
}

static int binarySearch(double* dist, double r, int first, int last)
{
    int mid = (last+first)/2;
    if(mid == 0)
        return 0;
    if(mid == first)
        return mid;
    else if(dist[mid] >= r && dist[mid-1] < r)
        return mid;
    else if(dist[mid] > r && dist[mid-1] > r)
        return binarySearch(dist, r, first, mid);
    else
        return binarySearch(dist, r, mid, last);
}

static double interpolation(double r, tabulated_pot_kernel* tab)
{
    if(r <= tab->dist[0])
        return tab->pot[0];
    else if(r >= tab->dist[tab->num-1])
        return tab->pot[tab->num-1];
    int idx = binarySearch(tab->dist, r, 0, tab->num);
    double y0 = tab->pot[(idx-1 < 0 ? 0 : idx-1)];
    double y1 = tab->pot[idx];
    double x0 = tab->dist[(idx-1 < 0 ? 0 : idx-1)];
    double x1 = tab->dist[idx];
    //printf("%lf %lf %lf\n", x0, r, x1);
    return y0+(y1-y0)/(x1-x0)*(r-x0);
}

double tabulated_pot_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src)
{
    tabulated_pot_kernel* kernel = (tabulated_pot_kernel*)src;
    double pos[3];
    for(size_t dim = 0; dim < 3; ++dim)
        pos[dim] = basis[dim][0]*i + basis[dim][1]*j + basis[dim][2]*k + origin[dim];
    double d = sqrt(pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2]);    
    
    return interpolation(d, kernel);
}

void tabulated_pot_destroy(void* src)
{
    free(((tabulated_pot_kernel*)src)->dist);
    free(((tabulated_pot_kernel*)src)->pot);
    free(src);
}
