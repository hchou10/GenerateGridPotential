#include "lenard_jones_trunc_kernel.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
struct lenard_jones_trunc_kernel
{
    double c6; 
    double c12;
    double scaling;
};

void* lenard_jones_trunc_create(char* name)
{
    void* src = malloc(sizeof(lenard_jones_trunc_kernel));
    FILE* infile = fopen(name, "r");
    if(infile == NULL)
    {
        perror("Error in opening file:");
        free(src);
        return NULL;
    }
    char buffer[512];
    ((lenard_jones_trunc_kernel*)src)->c6 = 0.;
    ((lenard_jones_trunc_kernel*)src)->c12 = 0.;
    ((lenard_jones_trunc_kernel*)src)->scaling = 1.; 
    while(fgets(buffer, 512, infile) != NULL)
    {
         char* tmp = strtok(buffer, " ");
         if(strcmp(tmp, "C6") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((lenard_jones_trunc_kernel*)src)->c6 = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }
         else if(strcmp(tmp, "C12") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((lenard_jones_trunc_kernel*)src)->c12 = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }
         else if(strcmp(tmp, "Scaling") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((lenard_jones_trunc_kernel*)src)->scaling = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }
         else
         {
             printf("Unrecognized pattern\n");
             free(src);
             return NULL;
         }
    }
    ((lenard_jones_trunc_kernel*)src)->c6 /= ((lenard_jones_trunc_kernel*)src)->scaling;
    ((lenard_jones_trunc_kernel*)src)->c12 /= ((lenard_jones_trunc_kernel*)src)->scaling;
    fclose(infile);
    return src;
}

double lenard_jones_trunc_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src)
{
    lenard_jones_trunc_kernel* kernel = (lenard_jones_trunc_kernel*)src;

    double sigma2 = pow(kernel->c12/kernel->c6,0.33333333333333333333);
    double epsilon = kernel->c6*kernel->c6/(4*kernel->c12);

    double pos[3];
    for(size_t dim = 0; dim < 3; ++dim)
        pos[dim] = basis[dim][0]*i + basis[dim][1]*j + basis[dim][2]*k + origin[dim];

    double d2 = pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2];
    double e;
    double rc2 = 2.5*2.5*sigma2;
    //rc2 = rc2*rc2*rc2;

    //d2 = d2*d2*d2;

    if(d2<=pow(2.,0.33333333333333333)*sigma2)
    {
        d2=d2*d2*d2;
        rc2=rc2*rc2*rc2;
        if(d2 > 0.)
        {
            e = 2.*(-(kernel->c6)/d2+(kernel->c12)/(d2*d2))+epsilon-((-kernel->c6)/rc2+(kernel->c12)/(rc2*rc2));
            return (e>200) ? 200 : e;
        }
        else
        {
            return 200;
        }
    }
    else if(d2>rc2)
        return 0;
    else
    {
        d2=d2*d2*d2;
        rc2=rc2*rc2*rc2;
        return -(kernel->c6)/d2+(kernel->c12)/(d2*d2)-((-kernel->c6)/rc2+(kernel->c12)/(rc2*rc2));
    }
}

void lenard_jones_trunc_destroy(void* src)
{
    free(src);
}
