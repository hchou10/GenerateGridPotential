#include "onc_elec_kernel.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#define CoulombConst 332.15417

struct onc_elec_kernel
{
    double kappa; 
    double Sz;
    double z;
    double q;
};

void* onc_elec_create(char* name)
{
    void* src = malloc(sizeof(onc_elec_kernel));
    FILE* infile = fopen(name, "r");
    if(infile == NULL)
    {
        perror("Error in opening file:");
        free(src);
        return NULL;
    }
    char buffer[512];
    while(fgets(buffer, 512, infile) != NULL)
    {
         char* tmp = strtok(buffer, " ");
         if(strcmp(tmp, "Kappa") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((onc_elec_kernel*)src)->kappa = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }
         else if(strcmp(tmp, "Sz") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((onc_elec_kernel*)src)->Sz = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }
         else if(strcmp(tmp, "Z") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((onc_elec_kernel*)src)->z = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         } 
         else if(strcmp(tmp, "Charge") == 0)
         {
             tmp = strtok(NULL, " ");
             if(tmp != NULL)
                 ((onc_elec_kernel*)src)->q = atof(tmp);
             else
             {
                 printf("Error formating\n");
                 free(src);
                 return NULL;
             }
         }  
         else
         {
             printf("Unrecognized pattern\n");
             free(src);
             return NULL;
         }
    }
    fclose(infile);
    return src;
}

static double computeDielectric(double d, onc_elec_kernel* pot)
{
    double z  = pot->z;
    double Sz = pot->Sz;
    double tmp = exp(d/z);
    return Sz*(1.-d*d/(z*z)*tmp/((tmp-1.)*(tmp-1.)));
}
/*
static double computeDerivativeDielectric(double d, onc_elec_kernel* pot)
{
    double z  = pot->z;
    double Sz = pot->Sz;
    double tmp = exp(d/z);
    double tmp1 = 1./(tmp-1.);
    return -Sz*d/(z*z)*tmp*tmp1*tmp1*(2.+d/z-2.*d/z*tmp*tmp1);
}
*/
double onc_elec_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src)
{
    onc_elec_kernel* kernel = (onc_elec_kernel*)src;
    double pos[3];
    for(size_t dim = 0; dim < 3; ++dim)
        pos[dim] = basis[dim][0]*i + basis[dim][1]*j + basis[dim][2]*k + origin[dim];
    double d = sqrt(pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2]);
    double constant = CoulombConst;
    double tmp = exp(-kernel->kappa*d);
    if(d == 0.)
        d = 1e-2;
    
    double er     = computeDielectric(d, kernel);
    return constant*tmp/(er*d)*kernel->q;
}

void onc_elec_destroy(void* src)
{
    free(src);
}
