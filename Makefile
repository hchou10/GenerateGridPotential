CC=gcc
INC=-I/usr/include -I./inc
ifeq ($(dbg),1)
	CFLAGS= -std=c99 -D_GNU_SOURCE -g -Og -Wall -Wextra -lm
else
	CFLAGS= -std=c99 -D_GNU_SOURCE -O3 -Wall -Wextra -lm
endif
##edit this if you have your own fft3
CLIB=-lfftw3
OBJ=main.o GridDxFormat/grid_reader.o Kernel/debye_huckel_kernel.o Kernel/lenard_jones_repulsion_kernel.o Kernel/lenard_jones_trunc_kernel.o \
LinkList/link_list.o fftw/fftw.o Kernel/lenard_jones_kernel.o Kernel/tabulated_pot_kernel.o Kernel/onc_elec_kernel.o Kernel/gaussian_kernel.o \
Kernel/lenard_jones_kernel1.o 

ifeq ($(dbg),1)
	EXE=gen_pot-dbg
else
	EXE=gen_pot
endif
EXE_SUM=sum_grid slice_grid sub_grid mult_grid

all: ${EXE} ${EXE_SUM}

${EXE}:${OBJ}
	${CC} ${CFLAGS} ${INC} ${OBJ} -o ${EXE} ${CLIB}

${EXE_SUM}: grid_tools/sum.o GridDxFormat/grid_reader.o grid_tools/slice.o grid_tools/sub.o grid_tools/multiply.o
	${CC} ${CFLAGS} ${INC} grid_tools/sum.o GridDxFormat/grid_reader.o -o sum_grid
	${CC} ${CFLAGS} ${INC} grid_tools/slice.o GridDxFormat/grid_reader.o -o slice_grid
	${CC} ${CFLAGS} ${INC} grid_tools/sub.o GridDxFormat/grid_reader.o -o sub_grid
	${CC} ${CFLAGS} ${INC} grid_tools/multiply.o GridDxFormat/grid_reader.o -o mult_grid
main.o: main.c
	${CC} ${CFLAGS} ${INC} -c main.c -o main.o

grid_tools/sub.o: grid_tools/sub.c
	${CC} ${CFLAGS} ${INC} -c grid_tools/sub.c -o grid_tools/sub.o

grid_tools/sum.o: grid_tools/sum.c
	${CC} ${CFLAGS} ${INC} -c grid_tools/sum.c -o grid_tools/sum.o

grid_tools/multiply.o: grid_tools/multiply.c
	${CC} ${CFLAGS} ${INC} -c grid_tools/multiply.c -o grid_tools/multiply.o

grid_tools/slice.o: grid_tools/slice.c
	${CC} ${CFLAGS} ${INC} -c grid_tools/slice.c -o grid_tools/slice.o

GridDxFormat/grid_reader.o: GridDxFormat/grid_reader.c
	${CC} ${CFLAGS} ${INC} -c GridDxFormat/grid_reader.c -o GridDxFormat/grid_reader.o

Kernel/debye_huckel_kernel.o:Kernel/debye_huckel_kernel.c
	${CC} ${CFLAGS} ${INC} -c Kernel/debye_huckel_kernel.c -o Kernel/debye_huckel_kernel.o

Kernel/lenard_jones_kernel.o:Kernel/lenard_jones_kernel.c
	${CC} ${CFLAGS} ${INC} -c Kernel/lenard_jones_kernel.c -o Kernel/lenard_jones_kernel.o

Kernel/lenard_jones_kernel1.o:Kernel/lenard_jones_kernel1.c
	${CC} ${CFLAGS} ${INC} -c Kernel/lenard_jones_kernel1.c -o Kernel/lenard_jones_kernel1.o

Kernel/lenard_jones_trunc_kernel.o:Kernel/lenard_jones_trunc_kernel.c
	${CC} ${CFLAGS} ${INC} -c Kernel/lenard_jones_trunc_kernel.c -o Kernel/lenard_jones_trunc_kernel.o

Kernel/lenard_jones_repulsion_kernel.o:Kernel/lenard_jones_repulsion_kernel.c
	${CC} ${CFLAGS} ${INC} -c Kernel/lenard_jones_repulsion_kernel.c -o Kernel/lenard_jones_repulsion_kernel.o

Kernel/tabulated_pot_kernel.o:Kernel/tabulated_pot_kernel.c
	${CC} ${CFLAGS} ${INC} -c Kernel/tabulated_pot_kernel.c -o Kernel/tabulated_pot_kernel.o

Kernel/onc_elec_kernel.o:Kernel/onc_elec_kernel.c
	${CC} ${CFLAGS} ${INC} -c Kernel/onc_elec_kernel.c -o Kernel/onc_elec_kernel.o

Kernel/gaussian_kernel.o:Kernel/gaussian_kernel.c
	${CC} ${CFLAGS} ${INC} -c Kernel/gaussian_kernel.c -o Kernel/gaussian_kernel.o

LinkList/link_list.o:LinkList/link_list.c
	${CC} ${CFLAGS} ${INC} -c LinkList/link_list.c -o LinkList/link_list.o

fftw/fftw.o:fftw/fftw.c
	${CC} ${CFLAGS} ${INC} -c fftw/fftw.c -o fftw/fftw.o

clean:
	rm -f ${OBJ} ${EXE} ${EXE_SUM} grid_tools/*.o
