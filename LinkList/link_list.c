#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include "link_list.h"
struct node
{
    void* key;
    void* val;
    struct node* next;
    //my_link_list* ptr;
};

struct link_list
{
    //append one empty node
    node* first;
    //node last; 

    default_constructor_type default_constructor_key, default_constructor_val;
    copy_constructor_type copy_constructor_key, copy_constructor_val;
    destructor_type destructor_key, destructor_val;
    compare_type compare;
};

void* link_list_create(default_constructor_type def_key, default_constructor_type def_val,
                          copy_constructor_type cpy_key,    copy_constructor_type cpy_val,
                          destructor_type destruct_key,     destructor_type destruct_val, compare_type comp)
{
    void* src = malloc(sizeof(link_list));
    link_list* ptr = (link_list*)src;
    ptr->default_constructor_key = def_key;
    ptr->default_constructor_val = def_val;
    ptr->copy_constructor_key    = cpy_key;
    ptr->copy_constructor_val    = cpy_val;
    ptr->destructor_key          = destruct_key;
    ptr->destructor_val          = destruct_val;
    ptr->compare                 = comp;
    ptr->first                   = (node*)malloc(sizeof(node));
    ptr->first->next = NULL;
    return src;
}

//on success return 0; otherwise return -1;
int insert_list(void* list, int pos, void* key, void* val)
{
    link_list* ptr = (link_list*)list;
    node* tmp = ptr->first;
    int count = 0;
    while(tmp->next != NULL && count != pos)
    {
        tmp = tmp->next;
        ++count;
    }
    node* insert_item = (node*)malloc(sizeof(node));
    
    if(insert_item != NULL)
    {
        insert_item -> key = ptr->copy_constructor_key(key);
        insert_item -> val = ptr->copy_constructor_val(val);
        node* old = tmp->next;
        tmp->next = insert_item;
        insert_item->next = old;
        return 0;
    }
    else
    {
        perror("Error malloc:");
        return -1;
    }
}

int append_list(void* list, void* key, void* val)
{
    link_list *ptr = (link_list*)list;
    node* tmp = ptr->first;
    while(tmp->next != NULL)
    {   
        tmp = tmp->next;
    }
    node* item = (node*)malloc(sizeof(node));
    if(item == NULL)
    {
       perror("Error in malloc:");
       return -1;
    }
    tmp -> next = item;
    item -> val = ptr->copy_constructor_val(val);
    item -> key = ptr->copy_constructor_key(key);
    item->next = NULL;
    return 0;
    //return tmp; //return NULL if not found
}

void* find_list(void* list, void* key)
{
    link_list *ptr = (link_list*)list;
    node* tmp = ptr->first->next;
    while(tmp != NULL)
    {
        if(ptr->compare(tmp->key, key) == 0)
            break;
        else
            tmp = tmp->next;
    }
    if(tmp != NULL)
        return tmp->val; //return NULL if not found
    else
        return NULL;
}

void clear_list(void* list)
{
    link_list *ptr = (link_list*)list;
    node* tmp = ptr->first->next;
    while(tmp != NULL)
    {
        ptr->destructor_val(tmp->val);
        ptr->destructor_key(tmp->key);
        node* old = tmp;
        tmp = tmp->next;
        free(old);
    }
    free(ptr->first);
    free(ptr);
}

void* string_default_constructor()
{
    return malloc(sizeof(char)*64);
}

void* string_copy_constructor(void* src)
{
    size_t len = strlen(src);
    void* dest = malloc(sizeof(char)*(len+1));
    strcpy(dest, src);
    return dest;
}

void string_destructor(void* src)
{
    free(src);
}

int string_compare(void* a, void* b)
{
    return strcmp(a,b);
}

void string_print(void* src)
{
    link_list *ptr = (link_list*)src;
    node* tmp = ptr->first->next;
    while(tmp != NULL) 
    {
        printf("Key: %s, Val: %s\n", (char*)(tmp->key), (char*)(tmp->val));
        tmp = tmp->next;
    }
}

void string_functor_print(void* src)
{
    link_list *ptr = (link_list*)src;
    node* tmp = ptr->first->next;
    while(tmp != NULL)
    {
        printf("Key: %s, Val: %p\n", (char*)(tmp->key), (char*)(tmp->val));
        tmp = tmp->next;
    }
}

void* shallow_default_constructor(void* src)
{
    return src;
}

void* shallow_copy_constructor(void* src)
{
    return src;
}

void shallow_destructor(void* src)
{
}


