This is a very small program for me to pre-compute potential file for ARBD simulation.
This program generates corresponding potential Dx file when a density Dx file and the potential type is given.
The supported potential type incudes 12-6 Lennard Jones, Debye-Huckel potential and tabulated potential.
I partially realize the structure of C++ class structure and late-binding using C function pointer and preprocessing. 
You have to install fftw3 in order to successfully compile the code.
I also provide some small tool to manipulate the Dx file such as mult_grid (multiply a grid by a constant), slice_grid (slice outside voxels), sub_grid (subtract two grid values),
sum_grid (sum two grids).