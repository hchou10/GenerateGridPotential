#include "kernel.h"
#include "link_list.h"
#include "grid_reader.h"
#include "fftw.h"
#include <stdio.h>
static void inner_product_grid(fftw_complex* dest, fftw_complex* src, size_t nx, size_t ny, size_t nz)
{
    double re, im;    
    size_t size = nx * ny * (nz/2+1);
    size_t V = nx*ny*nz;
    for(size_t i = 0; i < size; ++i)
    {
        re = dest[i][0];
        im = dest[i][1];
        size_t x = i / (nz/2+1) / ny; 
        size_t y = i / (nz/2+1) % ny;
        size_t z = i % (nz/2+1);
        dest[i][0] = (re*src[i][0]-im*src[i][1])/V*((0.5-(x+y+z)%2)*2);
        dest[i][1] = (re*src[i][1]+im*src[i][0])/V*((0.5-(x+y+z)%2)*2);
    }
}
static void shift_zero_frequency(size_t nx, size_t ny, size_t nz, double* data)
{
    for(size_t i = 0; i < nx; ++i)
    {
        for(size_t j = 0; j < ny; ++j)
        {
            for(size_t k = 0; k < nz; ++k)
            {
                size_t idx = k + nz * (j + ny * i);
                *(data+idx) = (*(data+idx))*2.*(0.5-(double)((i+j+k)%2));
            }
        }
    }
}
static void print_usage()
{
    printf("./exec input_density_dx_file potential_kernel_name kernel_init_file outpu_dx_file\n");
}
int main(int argc, char** argv)
{

    if(argc < 5)
    {
        printf("Invalid input arguments\n");
        print_usage();
        return -1;
    }
    link_list* compute_map = link_list_create(&shallow_default_constructor, &shallow_default_constructor,
                                              &shallow_copy_constructor,    &shallow_copy_constructor,
                                              &shallow_destructor,          &shallow_destructor, &string_compare);

    link_list* create_map = link_list_create(&shallow_default_constructor, &shallow_default_constructor,
                                             &shallow_copy_constructor,    &shallow_copy_constructor,
                                             &shallow_destructor,          &shallow_destructor, &string_compare);

    link_list* destroy_map = link_list_create(&shallow_default_constructor, &shallow_default_constructor,
                                              &shallow_copy_constructor,    &shallow_copy_constructor,
                                              &shallow_destructor,          &shallow_destructor, &string_compare);

    #define CreateType(key, val) \
    append_list(create_map, #key, &val)

    #define ComputeType(key, val) \
    append_list(compute_map,#key, &val)

    #define DestroyType(key, val) \
    append_list(destroy_map, #key, &val)

    #define KERNEL_TYPE
    #include "kernel.h"
    #undef KERNEL_TYPE
    #undef ComputeType 
    #undef CreateType
    #undef DestroyType

    char* density_file  = argv[1];
    char* kernel_name   = argv[2];
    char* kernel_infile = argv[3];

    void* density          = gridformat_reader(density_file);
    void* resample_density = resample_grid_data(density);
    grid_destroy(density);
    density = resample_density;

    void* potential = copy_grid_format(density);
   
    create_type creator = find_list(create_map, kernel_name);

    void* kernel = (*creator)(kernel_infile);

    compute_type functor = find_list(compute_map, kernel_name);
    set_grid_data(potential, functor, kernel);

    fftw_complex* density_out, *potential_out;
    fftw_plan plan1, plan2;

    density_out   = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (get_nx(density))*(get_ny(density))*(get_nz(density)/2+1));
    potential_out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (get_nx(density))*(get_ny(density))*(get_nz(density)/2+1));

    plan1 = fftwr2c_on_grid(density_out, get_nx(density), get_ny(density), get_nz(density), get_grid_data(density));

    fftw_execute(plan1);
    fftw_execute_dft_r2c(plan1, get_grid_data(potential), potential_out);

    inner_product_grid(potential_out, density_out, (get_nx(density)), (get_ny(density)), (get_nz(density)));

    plan2 = fftwc2r_on_grid(get_grid_data(potential), get_nx(density), get_ny(density), get_nz(density), potential_out);
 
    fftw_execute(plan2);
    gridformat_writer(argv[4], potential);
   
    //clean out all memory on the heap 
    destroy_type destroy = find_list(destroy_map, kernel_name);
    (*destroy)(kernel);

    clear_list(compute_map);
    clear_list(create_map);
    clear_list(destroy_map);        

    grid_destroy(density);
    grid_destroy(potential);
    fftw_free(density_out);
    fftw_free(potential_out);

    fftw_destroy_plan(plan1);
    fftw_destroy_plan(plan2);
    return 0;
}
