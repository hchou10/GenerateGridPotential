#ifdef KERNEL_TYPE
ComputeType(tabulated_pot, tabulated_pot_compute);
CreateType( tabulated_pot, tabulated_pot_create);
DestroyType(tabulated_pot, tabulated_pot_destroy);
#else
#ifndef TABULATED_POT_H_
#define TABULATED_POT_H_
#include <stddef.h>
typedef struct tabulated_pot_kernel tabulated_pot_kernel;
void* tabulated_pot_create(char* name);
double tabulated_pot_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src);
void tabulated_pot_destroy(void*);
#endif
#endif
