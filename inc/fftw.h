#ifndef FFTW_H_
#define FFTW_H_
#include <fftw3.h>
fftw_plan fftwr2c_on_grid(fftw_complex *out, size_t nx, size_t ny, size_t nz, double* data);
fftw_plan fftwc2r_on_grid(double* data, size_t nx, size_t ny, size_t nz, fftw_complex *out);
#endif
