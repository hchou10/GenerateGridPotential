#ifdef KERNEL_TYPE
ComputeType(lenard_jones_repulsion, lenard_jones_repulsion_compute);
CreateType(lenard_jones_repulsion, lenard_jones_repulsion_create);
DestroyType(lenard_jones_repulsion, lenard_jones_repulsion_destroy);
#else
#ifndef LENARD_JONES_REPULSION_H_
#define LENARD_JONES_REPULSION_H_
#include <stddef.h>
typedef struct lenard_jones_repulsion_kernel lenard_jones_repulsion_kernel;
void* lenard_jones_repulsion_create(char* name);
double lenard_jones_repulsion_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src);
void lenard_jones_repulsion_destroy(void*);
#endif
#endif
