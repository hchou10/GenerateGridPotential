#ifdef KERNEL_TYPE
ComputeType(lenard_jones, lenard_jones_compute);
CreateType(lenard_jones, lenard_jones_create);
DestroyType(lenard_jones, lenard_jones_destroy);
#else
#ifndef LENARD_JONES_H_
#define LENARD_JONES_H_
#include <stddef.h>
typedef struct lenard_jones_kernel lenard_jones_kernel;
void* lenard_jones_create(char* name);
double lenard_jones_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src);
void lenard_jones_destroy(void*);
#endif
#endif
