#ifdef KERNEL_TYPE
ComputeType(gaussian, gaussian_compute);
CreateType(gaussian, gaussian_create);
DestroyType(gaussian, gaussian_destroy);
#else
#ifndef GAUSSIAN_H_
#define GAUSSIAN_H_
#include <stddef.h>
typedef struct gaussian_kernel gaussian_kernel;
void* gaussian_create(char* name);
double gaussian_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src);
void gaussian_destroy(void*);
#endif
#endif
