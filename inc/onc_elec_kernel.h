#ifdef KERNEL_TYPE
ComputeType(onc_elec, onc_elec_compute);
CreateType(onc_elec, onc_elec_create);
DestroyType(onc_elec, onc_elec_destroy);
#else
#ifndef ONC_ELEC_H_
#define ONC_ELEC_H_
#include <stddef.h>
typedef struct onc_elec_kernel onc_elec_kernel;
void* onc_elec_create(char* name);
double onc_elec_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src);
void onc_elec_destroy(void*);
#endif
#endif
