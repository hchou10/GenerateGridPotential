#ifndef KERNEL_H_
#define KERNEL_H_
typedef void* (*create_type)(char*);
typedef void  (*destroy_type)(char*);
#endif
#include "debye_huckel_kernel.h"
#include "lenard_jones_kernel.h"
#include "lenard_jones_trunc_kernel.h"
#include "lenard_jones_repulsion_kernel.h"
#include "tabulated_pot_kernel.h"
#include "onc_elec_kernel.h"
#include "gaussian_kernel.h"
#include "lenard_jones_kernel1.h"
