#ifdef KERNEL_TYPE
ComputeType(lenard_jones1, lenard_jones_compute1);
CreateType(lenard_jones1, lenard_jones_create1);
DestroyType(lenard_jones1, lenard_jones_destroy1);
#else
#ifndef LENARD_JONES1_H_
#define LENARD_JONES1_H_
#include <stddef.h>
typedef struct lenard_jones_kernel1 lenard_jones_kernel1;
void* lenard_jones_create1(char* name);
double lenard_jones_compute1(size_t i, size_t j, size_t k, double* origin, double** basis, void* src);
void lenard_jones_destroy1(void*);
#endif
#endif
