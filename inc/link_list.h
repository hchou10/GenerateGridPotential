#ifndef LINK_LIST_H_
#define LINK_LIST_H_

typedef struct node node;
typedef struct link_list link_list;
typedef void* (*default_constructor_type)();
typedef void* (*copy_constructor_type)(void*);
typedef void  (*destructor_type)(void*); 
typedef int   (*compare_type)(void*, void*);
typedef void  (*print_type)(void*);

void* link_list_create(default_constructor_type def_key, default_constructor_type def_val,
                         copy_constructor_type cpy_key,     copy_constructor_type cpy_val,
                         destructor_type destruct_key,      destructor_type destruct_val, compare_type comp);


int insert_list(void* list, int pos, void* key, void* val);
int append_list(void* list, void* key, void* val);
void* find_list(void* list, void* key);
void clear_list(void* list);
void* string_default_constructor();
void* string_copy_constructor(void* src);
void string_destructor(void* src);
void string_print(void* src);
void string_functor_print(void* src);
int string_compare(void*, void*);
void* shallow_default_constructor(void* src);
void* shallow_copy_constructor(void* src);
void shallow_destructor(void*);

#endif
