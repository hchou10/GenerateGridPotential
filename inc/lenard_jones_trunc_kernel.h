#ifdef KERNEL_TYPE
ComputeType(lenard_jones_trunc, lenard_jones_trunc_compute);
CreateType(lenard_jones_trunc, lenard_jones_trunc_create);
DestroyType(lenard_jones_trunc, lenard_jones_trunc_destroy);
#else
#ifndef LENARD_JONES_TRUNC_H_
#define LENARD_JONES_TRUNC_H_
#include <stddef.h>
typedef struct lenard_jones_trunc_kernel lenard_jones_trunc_kernel;
void* lenard_jones_trunc_create(char* name);
double lenard_jones_trunc_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src);
void lenard_jones_trunc_destroy(void*);
#endif
#endif
