#ifdef KERNEL_TYPE
ComputeType(debye_huckel, debye_huckel_compute);
CreateType(debye_huckel, debye_huckel_create);
DestroyType(debye_huckel, debye_huckel_destroy);
#else
#ifndef DEBYE_HUCKEL_H_
#define DEBYE_HUCKEL_H_
#include <stddef.h>
typedef struct debye_huckel_kernel debye_huckel_kernel;
void* debye_huckel_create(char* name);
double debye_huckel_compute(size_t i, size_t j, size_t k, double* origin, double** basis, void* src);
void debye_huckel_destroy(void*);
#endif
#endif
